SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS my_guestbook;
DROP TABLE IF EXISTS my_info;
DROP TABLE IF EXISTS my_user;




/* Create Tables */

-- 留言本
CREATE TABLE my_guestbook
(
	id int NOT NULL AUTO_INCREMENT COMMENT 'id',
	info_id int COMMENT 'info_id',
	parent_id int COMMENT 'parent_id',
	from_user_id int COMMENT 'from_user_id',
	to_user_id int COMMENT 'to_user_id',
	content varchar(500) COMMENT 'content',
	del_flag int(1) COMMENT 'del_flag',
	create_date datetime COMMENT 'create_date',
	PRIMARY KEY (id)
) COMMENT = '留言本';


-- 信息表
CREATE TABLE my_info
(
	id int NOT NULL AUTO_INCREMENT COMMENT 'id',
	title varchar(200) COMMENT 'title',
	type int(2) COMMENT '类型  1招聘 2租房 3买卖 4活动 5问答',
	category varchar(100) COMMENT '类别',
	introduce varchar(500) COMMENT '简介',
	description text COMMENT '描述',
	price_range varchar(100) COMMENT '价格区间',
	price decimal(10,2) COMMENT '费用',
	amount int COMMENT '数量',
	company varchar(100) COMMENT '公司名称',
	address varchar(255) COMMENT '地址',
	contacts varchar(20) COMMENT '联系人',
	mobile varchar(20) COMMENT 'mobile',
	email varchar(32) COMMENT 'email',
	qq varchar(20) COMMENT 'qq',
	start_date datetime COMMENT 'start_date',
	end_date datetime COMMENT 'end_date',
	hits int COMMENT '点击数',
	pass_flag int(1) COMMENT '通过标记',
	del_flag int(1) COMMENT 'del_flag',
	create_by int COMMENT 'create_by',
	create_date datetime COMMENT 'create_date',
	update_date datetime COMMENT 'update_date',
	PRIMARY KEY (id)
) COMMENT = '信息表';


-- 用户表
CREATE TABLE my_user
(
	id int NOT NULL AUTO_INCREMENT COMMENT 'id',
	name varchar(20) COMMENT '名称',
	nick varchar(20) COMMENT '昵称',
	password varchar(128) COMMENT '密码',
	mobile varchar(20) COMMENT 'mobile',
	email varchar(32) COMMENT 'email',
	user_type int(1) COMMENT '用户类型 0普通用户 1管理员',
	login_ip varchar(64) COMMENT 'login_ip',
	login_date datetime COMMENT 'login_date',
	login_flag int(1) COMMENT 'login_flag',
	del_flag int(1) COMMENT 'del_flag',
	create_date datetime COMMENT 'create_date',
	wgateid varchar(128) COMMENT 'wgateid',
	open_id varchar(128) COMMENT 'open_id',
	sex int(1) COMMENT 'sex',
	city varchar(200) COMMENT 'city',
	province varchar(200) COMMENT 'province',
	country varchar(200) COMMENT 'country',
	headimgurl varchar(500) COMMENT 'headimgurl',
	PRIMARY KEY (id)
) COMMENT = '用户表';



