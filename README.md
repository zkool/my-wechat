基于微信公众号的简易的生活服务平台
适合未认证的订阅号，登录用到了第三方微信之门http://www.weixingate.com/

![示例公众号](http://git.oschina.net/uploads/images/2015/1209/084357_2794c4fc_1577.jpeg "示例公众号")

示例公众号截图如下：
![首页](http://git.oschina.net/uploads/images/2015/1209/084747_067efecc_1577.png "首页")

![详情页](http://git.oschina.net/uploads/images/2015/1209/084851_19dc15e2_1577.png "详情页")

![详情页留言](http://git.oschina.net/uploads/images/2015/1209/084833_e51fcccf_1577.png "详情页留言")

![我的页面](http://git.oschina.net/uploads/images/2015/1209/084949_39b26e96_1577.png "我的页面")

![我的发布页面](http://git.oschina.net/uploads/images/2015/1209/085013_5f450f30_1577.png "我的发布页面")

联系QQ：987617382