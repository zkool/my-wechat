package com.zkool.wechat.util;

import java.util.HashMap;

public class JsonVo {

	public JsonVo(int code){
		super();
		this.code = code;
	}
	
	public JsonVo(int code, String msg){
		super();
		this.code = code;
		this.msg = msg;
	}
	
	public JsonVo(int code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	
	public JsonVo(int code, String msg, HashMap<String, String> errors) {
		super();
		this.code = code;
		this.msg = msg;
		this.errors = errors;
	}

	/**
	 * 状态码
	 */
	private int code;
	
	/**
	 * 成功的消息
	 */
	private String msg;
	
	private Object data;
	
	/**
	 * 具体每个输入错误的消息
	 */
	private HashMap<String, String> errors = new HashMap<String, String>();

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public HashMap<String, String> getErrors() {
		return errors;
	}

	public void setErrors(HashMap<String, String> errors) {
		this.errors = errors;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
