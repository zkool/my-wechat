package com.zkool.wechat.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DateUtil {

	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String DATE_NOLINE = "yyyyMMdd";
	public static final String TIME_FORMAT = "HH:mm:ss";
	public static final String DATA_CN = "yyyy年MM月dd日";
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.S";
	
	/**
	 * 获取当前Timestamp时间
	 * 
	 * @return 返回当前时间Timestamp
	 */
	public static java.sql.Timestamp newTime() {
		return new java.sql.Timestamp(System.currentTimeMillis());
	}

	/**
	 * 获取当前Date时间
	 * 
	 * @return 返回当前时间Date
	 */
	public static java.sql.Date newDate() {
		return new java.sql.Date(System.currentTimeMillis());
	}

	/**
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static String format(java.util.Date date, String dateFormat) {
		if (date == null)
			return null;
		return new SimpleDateFormat(dateFormat).format(date);
	}

	/**
	 * @param dateString
	 * @param dateFormat
	 * @return
	 */
	public static java.util.Date parse(String dateString, String dateFormat) {
		return parse(dateString, dateFormat, java.util.Date.class);
	}

	
	@SuppressWarnings("unchecked")
	public static <T extends java.util.Date> T parse(String dateString,
			String dateFormat, Class<T> targetResultType) {
		if (dateString == null || dateString.length() == 0)
			return null;
		DateFormat df = new SimpleDateFormat(dateFormat);
		try {
			long time = df.parse(dateString).getTime();
			java.util.Date t = targetResultType.getConstructor(long.class)
					.newInstance(time);
			return (T) t;
		} catch (ParseException e) {
			String errorInfo = "cannot use dateformat:" + dateFormat
					+ " parse datestring:" + dateString;
			throw new IllegalArgumentException(errorInfo, e);
		} catch (Exception e) {
			throw new IllegalArgumentException("error targetResultType:"
					+ targetResultType.getName(), e);
		}
	}

	/**
	 * 获取本周一
	 * 
	 * @return
	 */
	public static String nowMonday() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return format(calendar.getTime(), DATE_FORMAT);
	}

	/**
	 * 获取本周日
	 * 
	 * @return
	 */
	public static String nowSunday() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);
		int dayofweek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (dayofweek == 0)
			dayofweek = 7;
		calendar.add(Calendar.DATE, -dayofweek + 7);
		return format(calendar.getTime(), DATE_FORMAT);
	}

	/**
	 * 获取本月的第一天
	 * 
	 * @return
	 */
	public static String monthFirstDay() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return format(calendar.getTime(), DATE_FORMAT);
	}

	/**
	 * 获取本月的最后一天
	 * 
	 * @return
	 */
	public static String monthLastDay() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
		calendar.set(Calendar.DAY_OF_MONTH, 0);
		return format(calendar.getTime(), DATE_FORMAT);
	}

	/**
	 * 获取明天
	 * 
	 * @return
	 */
	@SuppressWarnings("static-access")
	public static String tomorrow() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);
		calendar.add(calendar.DATE, 1);// 把日期往后增加一天.整数往后推,负数往前移动
		return format(calendar.getTime(), DATE_FORMAT);
	}
    /**
     * 获取今天
     */
	public static String Today(){
		return format(newTime(),DATE_FORMAT);
	}
	/**
	 * 获取星期
	 * 
	 * @param date
	 * @return
	 */
	public static String getWeek(java.util.Date date) {
		String week;
		switch (date.getDay()) {
		case 1:
			week = "星期一";
			break;
		case 2:
			week = "星期二";
			break;
		case 3:
			week = "星期三";
			break;
		case 4:
			week = "星期四";
			break;
		case 5:
			week = "星期五";
			break;
		case 6:
			week = "星期六";
			break;
		default:
			week = "星期天";
		}
		return week;
	}
	
	/**
	 * 按天将时间分组
	 * @param start
	 * @param end
	 * @param days
	 * @return
	 */
	public static List<Date[]> splitTimeByDays(Date start, Date end, int days) {
		return splitTimeByHours(start, end, 24 * days);
	}

	/**
	 * 按小时将时间分组
	 * @param start
	 * @param end
	 * @param hours
	 * @return
	 */
	public static List<Date[]> splitTimeByHours(Date start, Date end, int hours) {
		List<Date[]> dl = new ArrayList<Date[]>();
		while (start.compareTo(end) < 0) {
			Date _end = addHours(start, hours);
			if (_end.compareTo(end) > 0) {
				_end = end;
			}
			Date[] dates = new Date[] { (Date) start.clone(), (Date) _end.clone() };
			dl.add(dates);

			start = _end;
		}
		return dl;
	}
	
	/**
	 * 按秒 加减时间
	 * @param date
	 * @param second
	 * @return
	 */
	public static Date addSeconds(Date date, int second) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.SECOND, second);
		return cal.getTime();
	}
	
	/**
	 * 按分 加减时间
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addMinutes(Date date, int amount) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MINUTE, amount);
		return c.getTime();
	}
	
	/**
	 * 按小时 加减时间
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addHours(Date date, int amount) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR_OF_DAY, amount);
		return c.getTime();
	}
	
	/**
	 * 按天 加减时间
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addDays(Date date, int amount) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, amount);
		return c.getTime();
	}
	
	/**
	 * 按月 加减时间
	 * @param date
	 * @param amount
	 * @return
	 */
	public static Date addMonths(Date date, int amount) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, amount);
		return c.getTime();
	}
	
	/**
	 * 获取今天的开始时刻。
	 */
	public static Date getTodayStartTime() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	/** 
     * 返回两个日期相差的天数 
     * @param startDate 
     * @param endDate 
     * @return 
     * @throws ParseException 
     */  
    public static int getDistDays(Date startDate,Date endDate){  
        long totalDate = 0;  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(startDate);  
        long timestart = calendar.getTimeInMillis();  
        calendar.setTime(endDate);  
        long timeend = calendar.getTimeInMillis();  
        totalDate = Math.abs((timeend - timestart))/(1000*60*60*24);  
        return Integer.parseInt(String.valueOf(totalDate)); 
    }   
	
	/**   
     * 两个日期之间相差的秒数   
     * @param startDate   
     * @param endDate   
     * @return   
     */    
    public static int getDistSeconds(Date startDate,Date endDate){
    	long totalDate = 0;  
        Calendar cal = Calendar.getInstance();     
        cal.setTime(startDate);     
        long timestart = cal.getTimeInMillis();                  
        cal.setTime(endDate);     
        long timeend = cal.getTimeInMillis();          
        totalDate = Math.abs((timeend - timestart))/(1000);  
        return Integer.parseInt(String.valueOf(totalDate)); 
    } 
}
