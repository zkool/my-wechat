package com.zkool.wechat.controller;

import java.util.Date;

import com.jfinal.core.Controller;
import com.zkool.wechat.model.GuestBook;
import com.zkool.wechat.model.User;
import com.zkool.wechat.util.JsonVo;

public class GuestBookController extends Controller{

	/**
	 * ajax获取info留言列表
	 */
	public void guestBookList() {
		int info_id = getParaToInt("info_id");
		renderJson(new JsonVo(200, "success", GuestBook.me.paginate(getParaToInt("page", 1), 10, info_id)));
	}
	
	public void save(){
		GuestBook guestBook = getModel(GuestBook.class);
		int user_id = 0;
		User loginUser = getSessionAttr("wechat_user");
		if(loginUser != null){
			user_id = loginUser.getInt("id");
		}
		guestBook.set("from_user_id", user_id);
		guestBook.set("del_flag", 0);
		guestBook.set("create_date", new Date());
		guestBook.save();
		renderJson(new JsonVo(200, "success", guestBook));
	}
	
	/**
	 * 删除：逻辑删除
	 */
	public void delete() {
		int id = getParaToInt("id");
		new GuestBook().set("id", id).set("del_flag", 1).update();
		renderJson(new JsonVo(200, "success"));
	}
	
}
