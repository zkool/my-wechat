package com.zkool.wechat.controller;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.zkool.wechat.model.Info;
import com.zkool.wechat.model.User;
import com.zkool.wechat.util.JsonVo;

/**
 * 我的
 * @author zkool
 *
 */
public class MyController extends Controller {

	public void index() {
		render("index.html");
	}
	
	/**
	 * 我的发布
	 */
	public void list() {
		int user_id = 0;
		User loginUser = getSessionAttr("wechat_user");
		if(loginUser != null){
			user_id = loginUser.getInt("id");
		}
		setAttr("infoPage", Info.me.paginateByCreater(getParaToInt(0,1), 10, user_id));
		render("list.html");
	}
	
	/**
	 * 我的发布ajax分页获取
	 */
	public void listAjax() {
		int user_id = 0;
		User loginUser = getSessionAttr("wechat_user");
		if(loginUser != null){
			user_id = loginUser.getInt("id");
		}
		renderJson(new JsonVo(200, "success", Info.me.paginateByCreater(getParaToInt("page", 1), 10, user_id)));
	}
	
	/**
	 * 我的发布明细
	 */
	public void detail() {
		setAttr("info", Info.me.findById(getParaToInt()));
		render("detail.html");
	}
	
	/**
	 * 待审核列表
	 */
	public void waitExamine() {
		User loginUser = getSessionAttr("wechat_user");
		Page<Info> infoPage = null;
		if(loginUser != null){
			infoPage = Info.me.paginateByWaitExamine(getParaToInt(0, 1), 10);
		}
		setAttr("infoPage", infoPage);
		render("waitExamine.html");
	}
	
	/**
	 * 待审核列表ajax分页获取
	 */
	public void waitExamineAjax() {
		User loginUser = getSessionAttr("wechat_user");
		Page<Info> infoPage = null;
		if(loginUser != null){
			infoPage = Info.me.paginateByWaitExamine(getParaToInt(0, 1), 10);
		}
		renderJson(new JsonVo(200, "success", infoPage));
	}
	
	/**
	 * 待审核列表对应的明细
	 */
	public void waitExamineDetail() {
		setAttr("info", Info.me.findById(getParaToInt()));
		render("waitExamineDetail.html");
	}

}
