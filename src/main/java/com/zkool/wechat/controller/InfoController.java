package com.zkool.wechat.controller;

import java.util.Date;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.zkool.wechat.model.Info;
import com.zkool.wechat.model.User;
import com.zkool.wechat.util.DateUtil;
import com.zkool.wechat.util.JsonVo;

/**
 * 外出
 * @author zkool
 *
 */
public class InfoController extends Controller {

	/**
	 * 添加或编辑
	 */
	public void edit(){
		Integer id = getParaToInt("id");
		if(id != null && id.intValue() > 0){
			setAttr("info", Info.me.findById(id));
		}
		String page = "edit_" + getParaToInt("type", 1) + ".html";
		render(page);
	}
	
	/**
	 * 保存或更新
	 */
	public void save() {
		Info info = getModel(Info.class);
		String startDateStr = getPara("startDateStr");
		String endDateStr = getPara("endDateStr");
		if(StrKit.notBlank(startDateStr)){
			info.set("start_date", DateUtil.parse(startDateStr,DateUtil.DATE_NOLINE));
		}
		if(StrKit.notBlank(endDateStr)){
			info.set("end_date", DateUtil.parse(endDateStr, DateUtil.DATE_NOLINE));
		}
		Date currDate = new Date();
		info.set("update_date", currDate);
		if(info.getInt("id") != null){
			info.update();
		}else{
			int user_id = 0;
			User loginUser = getSessionAttr("wechat_user");
			if(loginUser != null){
				user_id = loginUser.getInt("id");
			}
			info.set("create_by", user_id);
			info.set("hits", 0);
			info.set("pass_flag", 0);
			info.set("del_flag", 0);
			info.set("create_date", currDate);
			info.save();
		}
		renderJson(new JsonVo(200, "success"));
	}
	
	/**
	 * 删除：逻辑删除
	 */
	public void delete() {
		int id = getParaToInt("id");
		new Info().set("id", id).set("del_flag", 1).update();
		renderJson(new JsonVo(200, "success"));
	}
	
	/**
	 * 审核：1通过；-1不通过
	 */
	public void pass() {
		int id = getParaToInt("id");
		int pass_flag = getParaToInt("pass_flag");
		new Info().set("id", id).set("pass_flag", pass_flag).update();
		renderJson(new JsonVo(200, "success"));
	}
	
}
