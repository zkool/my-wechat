package com.zkool.wechat.controller;

import com.jfinal.core.Controller;
import com.zkool.wechat.model.GuestBook;
import com.zkool.wechat.model.Info;
import com.zkool.wechat.util.JsonVo;

public class IndexController extends Controller{

	/**
	 * 按分类查询列表
	 */
	public void index() {
		int type = getParaToInt("type", 1);
		setAttr("infoPage", Info.me.paginate(getParaToInt("page", 1), 10, type));
		setAttr("type", type);
		render("index.html");
	}
	
	public void list() {
		int type = getParaToInt("type", 1);
		renderJson(new JsonVo(200, "success", Info.me.paginate(getParaToInt("page", 1), 10, type)));
	}
	
	public void detail() {
		setAttr("info", Info.me.findById(getParaToInt(0)));
		setAttr("type", getParaToInt(1));
		setAttr("guestBookPage", GuestBook.me.paginate(getParaToInt("page", 1), 10, getParaToInt(0)));
		render("detail.html");
	}
	
}
