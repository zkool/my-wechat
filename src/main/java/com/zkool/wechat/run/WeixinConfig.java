/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.zkool.wechat.run;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.zkool.wechat.controller.GuestBookController;
import com.zkool.wechat.controller.IndexController;
import com.zkool.wechat.controller.InfoController;
import com.zkool.wechat.controller.MyController;
import com.zkool.wechat.controller.UserController;
import com.zkool.wechat.controller.WeixinApiController;
import com.zkool.wechat.controller.WeixinMsgController;
import com.zkool.wechat.model.GuestBook;
import com.zkool.wechat.model.Info;
import com.zkool.wechat.model.User;

public class WeixinConfig extends JFinalConfig {
	
	/**
	 * 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
	 * @param pro 生产环境配置文件
	 * @param dev 开发环境配置文件
	 */
	public void loadProp(String pro, String dev) {
		try {
			PropKit.use(pro);
		}
		catch (Exception e) {
			PropKit.use(dev);
		}
	}
	
	public void configConstant(Constants me) {
		loadProp("a_little_config_pro.txt", "a_little_config.txt");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		
		// ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
		ApiConfigKit.setDevMode(me.getDevMode());
		
		me.setBaseViewPath("/WEB-INF/views");
	}
	
	public void configRoute(Routes me) {
		me.add("/", IndexController.class,"/index");
		me.add("/msg", WeixinMsgController.class);
		me.add("/api", WeixinApiController.class, "/api");
		me.add("/info", InfoController.class, "/info");
		me.add("/user", UserController.class, "/user");
		me.add("/guestbook", GuestBookController.class, "/guestbook");
		me.add("/my", MyController.class);
	}
	
	public void configPlugin(Plugins me) {
		 C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		 me.add(c3p0Plugin);
		
		 EhCachePlugin ecp = new EhCachePlugin();
		 me.add(ecp);
		 
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.addMapping("my_user", User.class);	// 映射my_user 表到 User模型
		arp.addMapping("my_info", Info.class);
		arp.addMapping("my_guestbook", GuestBook.class);
	}
	
	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor(true));
	}
	
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler());
	}
	
	public static void main(String[] args) {
		JFinal.start("src/main/webapp", 80, "/", 5);
	}
}
