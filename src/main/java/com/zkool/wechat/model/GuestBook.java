package com.zkool.wechat.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class GuestBook extends Model<GuestBook>{

	public static final GuestBook me = new GuestBook();
	
	private String fromUserName;
	private String toUserName;
	
	//回复列表
	private List<GuestBook> childGuestBook;
	
	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public List<GuestBook> getChildGuestBook() {
		return childGuestBook;
	}

	public void setChildGuestBook(List<GuestBook> childGuestBook) {
		this.childGuestBook = childGuestBook;
	}

	/**
	 * 设置用户名称
	 * @param guestBook
	 */
	public void setGuestBookUsers(GuestBook guestBook){
		if(guestBook.getInt("from_user_id") != null && guestBook.getInt("from_user_id") > 0){
			guestBook.setFromUserName(User.me.findByIdLoadColumns(guestBook.get("from_user_id"), "nick").get("nick").toString());
		}
		
		if(guestBook.getInt("to_user_id") != null && guestBook.getInt("to_user_id") > 0){
			guestBook.setToUserName(User.me.findByIdLoadColumns(guestBook.get("to_user_id"), "nick").get("nick").toString());
		}
	}
	
	/**
	 * 按父id查询
	 * @param parent_id
	 * @return
	 */
	public List<GuestBook> getChildGuestBookByPid(int parent_id) {
		List<GuestBook> list = find("select * from my_guestbook where del_flag=0 and parent_id=? order by id asc", parent_id);
		for (GuestBook guestBook : list) {
			setGuestBookUsers(guestBook);
		}
		return list;
	}
	
	/**
	 * 查询留言列表
	 * @param pageNumber
	 * @param pageSize
	 * @param infoId
	 * @return
	 */
	public Page<GuestBook> paginate(int pageNumber, int pageSize, int infoId) {
		Page<GuestBook> result = paginate(pageNumber, pageSize, "select *", "from my_guestbook where info_id=? and parent_id=0 and del_flag=0 order by id asc", infoId);
		if(result != null && result.getList() != null){
			for (GuestBook guestBook : result.getList()) {
				setGuestBookUsers(guestBook);
				guestBook.setChildGuestBook(getChildGuestBookByPid(guestBook.getInt("id")));
			}
		}
		return result;
	}
	
}
