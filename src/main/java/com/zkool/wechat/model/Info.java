package com.zkool.wechat.model;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.zkool.wechat.util.DateUtil;

@SuppressWarnings("serial")
public class Info extends Model<Info>{

	public static final Info me = new Info();
	
	private String startDateStr;
	private String endDateStr;
	private String createDateStr;
	

	public String getStartDateStr() {
		if(StrKit.notNull(this.getDate("start_date"))){
			startDateStr = DateUtil.format(this.getDate("start_date"), DateUtil.DATE_NOLINE);
		}
		return startDateStr;
	}


	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}


	public String getEndDateStr() {
		if(StrKit.notNull(this.getDate("end_date"))){
			endDateStr = DateUtil.format(this.getDate("end_date"), DateUtil.DATE_NOLINE);
		}
		return endDateStr;
	}


	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getCreateDateStr() {
		if(StrKit.notNull(this.getDate("create_date"))){
			createDateStr = DateUtil.format(this.getDate("create_date"), DateUtil.DATA_CN);
		}
		return createDateStr;
	}
	
	public Page<Info> paginate(int pageNumber, int pageSize) {
		return paginate(pageNumber, pageSize, "select *", "from my_info order by id desc");
	}
	
	/**
	 * 首页按类型查询：未删除的，审核通过的
	 * @param pageNumber
	 * @param pageSize
	 * @param type
	 * @return
	 */
	public Page<Info> paginate(int pageNumber, int pageSize, int type) {
		return paginate(pageNumber, pageSize, "select *", "from my_info where del_flag=0 and pass_flag=1 and type=? order by update_date desc", type);
	}
	
	/**
	 * 查询自己发布的：未删除的
	 * @param pageNumber
	 * @param pageSize
	 * @param create_id
	 * @return
	 */
	public Page<Info> paginateByCreater(int pageNumber, int pageSize, int create_id) {
		return paginate(pageNumber, pageSize, "select *", "from my_info where del_flag=0 and create_by=? order by id desc", create_id);
	}
	
	/**
	 * 待审核列表：未删除的，未审核通过的
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Info> paginateByWaitExamine(int pageNumber, int pageSize){
		return paginate(pageNumber, pageSize, "select *", "from my_info where del_flag=0 and pass_flag!=1 order by update_date desc");
	}
	
}
